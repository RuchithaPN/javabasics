package operators;
// Increment operator ++ ... Decrement operator --

public class IncrementDecrement {

	public static void main(String[] args) {
      int i = 100;
      int j=200;
      
      System.out.println(i); // i value is 100
      System.out.println(++i);  // 100+1 = 101
      System.out.println(--i); // 101-1= 100
      System.out.println(++j);  // 200+1=201
	}

}
