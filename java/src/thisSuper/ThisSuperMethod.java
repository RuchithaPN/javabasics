package thisSuper;
class Parent{
	void Play() {
		System.out.println("This is parent class.....");
	}
}

public class ThisSuperMethod extends Parent {
	void Play() {
		System.out.println("This is current class......");
	}
     void all() {  // calling Current class Play method and Parent class Play method
    	 super.Play(); //Parent class Play method
    	 this.Play(); // Current class Play method
     }
     public static void main(String[] args) {
    	 ThisSuperMethod obj=new ThisSuperMethod();
    	 obj.all();

	}
}
