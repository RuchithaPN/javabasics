package thisSuper;

class MyParent{
	String name="This is Parent class instant variable....";
}

public class MyClass extends MyParent{
	String name="This is instant variable....";
	
	void somemethod() {
		String name="This is local variable... because it is inside the method";
		System.out.println(name);
		System.out.println(this.name);
		System.out.println(super.name);
		
	}

	public static void main(String[] args) {
		MyClass mc=new MyClass();
		mc.somemethod();

	}

}
