package thisSuper;

class Animal{
	String color="white....";  // Parent class instant variable
}
public class Dog extends Animal {
	String color="Green...."; // current or present class instant variable
	
	void printcolor() {
		String color="Black....."; // local variable
		System.out.println(color); //It prints local variable
		System.out.println(this.color); // It prints current class instant variable
		System.out.println(super.color); //It prints parent class instant variable
	}

	public static void main(String[] args) {
		Dog d=new Dog();
		d.printcolor();

	}

}
