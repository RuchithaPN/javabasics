// To Find Smallest Element
package assignment;

import java.util.Arrays;

public class assign2 {
	public static int getSmallest(int[] a){
		int temp;
		for(int i=0;i<a.length;i++)
		{
			for(int j=i+1;j<a.length;j++)
			{
				if(a[i]>a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		return a[0];
	}

	public static void main(String[] args) {
     int[] a= {33,23,4,6,8,5};
     System.out.println("Smallest:" + getSmallest(a));
     
     
     
     int number[]= {24,89,78,56,1,};
     Arrays.sort(number);
     System.out.println("Smallest: " +number[0]);
     
	}

}
