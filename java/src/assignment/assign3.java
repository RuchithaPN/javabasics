//Find duplicate characters in a string
package assignment;

import java.util.Scanner;

public class assign3 {

	public static void main(String[] args) {
		//String str="beautiful";
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter original String is:");
		String str=sc.next();
		char[] carray=str.toCharArray();
		
		System.out.println("The original String is:" +str);
		System.out.println("Duplicate Characters in the given String is:");
		
		for(int i=0;i<str.length();i++) {
			for(int j=i+1;j<str.length();j++) {
				
				if(carray[i]==carray[j]) {
					System.out.println(carray[j]+" ");
					break;
				}
			}
		}
	}

}
