package conditionalStatements;

import java.util.Scanner;

public class IfElseDemo {

	public static void main(String[] args) {

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the age....");
		int age = sc.nextInt();
		
		if(age>=20 && age<=40) {
			System.out.println("Your are Adult......");
		}
		else if(age>40){
			System.out.println("You are Old.....");
		}
		else {
			System.out.println("Your are minor.....");
		}
	}

}
