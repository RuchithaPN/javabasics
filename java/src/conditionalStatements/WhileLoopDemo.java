package conditionalStatements;

public class WhileLoopDemo {

	public static void main(String[] args) {
      int a=1;   // Initialization
      while(a<=10) {
    	  System.out.println(a);
    	  a++;
      }
	}

}
