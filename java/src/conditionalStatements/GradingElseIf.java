package conditionalStatements;

import java.util.Scanner;

public class GradingElseIf {

	public static void main(String[] args) {
      Scanner sc= new Scanner(System.in);
      System.out.println("Enter Science marks....");
      int science = sc.nextInt();
      
      System.out.println("Enter Maths marks....");
      int maths = sc.nextInt();
      
      System.out.println("Enter English marks....");
      int english = sc.nextInt();
      
      int total= science+maths+english;// add all subject marks to variable 'total'
      System.out.println("Grand total:" +total);
      
      float avg = total/3;
      System.out.println("Average score is:" +avg);
      
      if(avg>=90) {
    	  System.out.println("Congrats... You have secured A grade...");
      }
      else if(avg>=80 && avg<90) {
    	  System.out.println("You have secured B grade... Can improve...");
      }
      else if(avg>=70 && avg<80) {
    	  System.out.println("You have secured C grade... Work hard...");
      }
      else {
    	  System.out.println("Failed.... Try again.....");
      }
	}

}
