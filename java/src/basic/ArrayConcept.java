package basic;

public class ArrayConcept {

	public static void main(String[] args) {
// array can store multiple values
		
		int[] age= { 10,20,30,40,50,60};
		System.out.println(age[1]);
		System.out.println(age.length);
		
		String[] namesOfStudents= {"Ram","Lucky","Sona"};
		System.out.println(namesOfStudents[1]);
		
		String[] names= new String[10];
		names[0]="San";
		names[1]="shrav";
		names[2]="xen";
		System.out.println(names[2]);
	}

}
