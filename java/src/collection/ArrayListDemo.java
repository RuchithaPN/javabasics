package collection;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
      ArrayList<Object> list=new ArrayList<>();
      
      list.add(345);
      list.add(2436.325);
      list.add("Ram");
      list.add(1);
      System.out.println(list);
	}

}
