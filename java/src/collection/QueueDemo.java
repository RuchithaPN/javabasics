package collection;

import java.util.LinkedList;
import java.util.Queue;

public class QueueDemo {

	public static void main(String[] args) {
      Queue<Integer> num=new LinkedList<>();
    	  
      num.offer(100);
      num.offer(200);
      num.offer(300);
      num.offer(400);
      num.offer(500);
      num.offer(600);
      System.out.println(num);
      int num1=num.peek();
      System.out.println("The head of the queue is:" +num1);
      int num2=num.poll();
      System.out.println("Removed item from the queue is:" +num2);
      System.out.println(num);
      
      }
	}


