package collection;

import java.util.Collections;
import java.util.PriorityQueue;

public class PriorityQueueDemo {

	public static void main(String[] args) {
      PriorityQueue<Integer> num=new PriorityQueue<>(Collections.reverseOrder());
      
      num.offer(10);
      num.offer(20);
      num.offer(70);
      num.offer(40);
      num.offer(100);
      num.offer(510);
      num.offer(666);
      num.offer(750);
      num.offer(810);
      num.offer(999);
      System.out.println(num);
      
      int s=num.size();
      while(s>0) {
    	  System.out.println(num.remove()+" ");
      }
	}

}
