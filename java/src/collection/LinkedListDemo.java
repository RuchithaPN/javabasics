package collection;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
       LinkedList<String> animals=new LinkedList<>();
       
       animals.add("cat");
       animals.add("dog");
       
       System.out.println(animals);
       animals.add(0,"elephant");
       System.out.println(animals);
       
       animals.addLast("tiger");
       System.out.println(animals);
       
       animals.addFirst("lion");
       System.out.println(animals);
       
       String s=animals.get(1);
       System.out.println(s);
       
       LinkedList<String> fruits=new LinkedList<>();
       
       fruits.add("apple");
       fruits.add("mango");
       
       animals.addAll(fruits);
       System.out.println(animals);
       
	}

}
