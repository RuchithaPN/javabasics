package InterfaceDemo;
//Multiple inheritance achieved with interface
interface Continent{
	void continentName(); // abstract method
	
}
interface Country{
	void countryName(); // abstract method
	
}
interface City{
	void cityName(); // abstract method
	
}

public class Multi implements Continent,Country, City{

	
	@Override
	public void cityName() {
		System.out.println("Mysore");
	}

	@Override
	public void countryName() {
		System.out.println("India");
		
	}

	@Override
	public void continentName() {
		System.out.println("Asia");
	}

	public static void main(String[] args) {
         Multi m=new Multi();
         m.cityName();
         m.countryName();
         m.continentName();
	}

}
