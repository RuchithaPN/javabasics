package InterfaceDemo;

public class B implements A {
	


	@Override
	public void method1() { // abstract method from interface A
		System.out.println("This is interface method1.......");
		
	}

	@Override
	public void method2() { //// abstract method from interface A
		System.out.println("This is interface method2.....");
		
	}
	void thanks() { // this is my own method
        System.out.println("This is my own method.......");		
	}

	public static void main(String[] args) {
		B obj=new B();
		obj.method1();
		obj.method2();
		obj.thanks();
		

	}
}
