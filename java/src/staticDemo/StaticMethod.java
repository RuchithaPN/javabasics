package staticDemo;

public class StaticMethod {
	
	void play() {    //Regular method
		System.out.println("I like to play badmition......");
	}
	
	// Static method
	static void sleep() {
		System.out.println("I sleep at 11.....");  // no need to create object to call static method
	}

	public static void main(String[] args) {
    sleep();
    StaticMethod sm=new StaticMethod();
    sm.play();
	}

}
