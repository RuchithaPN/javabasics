package polymorphism;

public class Main {

	public static void main(String[] args) {
		Animal a = new Animal();
		Dog d= new Dog();
		Pig p=new Pig();
		a.sound(); // calling parent sound method
        d.sound();//  calling child sound method
        p.sound();// calling another child sound method
	}

}
