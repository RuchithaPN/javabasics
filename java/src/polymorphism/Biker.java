package polymorphism;
// Overriding Method
// Creating Parent Class
 class vehicle {
	 // Defining a method
	 void run() {
		System.out.println("Vehicle is running...");

	}

}
//Creating a Child class
class Biker extends vehicle{
	// Defining the same method as parent class
	void run() {
		System.out.println("bike is running...");
	}
	public static void main(String args[]) {
	
	Biker b= new Biker(); // creating object
	b.run();// calling method
	}
}