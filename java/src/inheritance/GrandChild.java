package inheritance;

public class GrandChild extends Child { // inherits from child class
	
	public void gcmethod() {
		System.out.println("This is Gradchild....");
	}

	public static void main(String[] args) {
		GrandChild g=new GrandChild();
		g.method1();  // calling parent method1
		g.method2();  // calling parent method2
		g.mymethod(); // calling method from child class

	}

}
