package inheritance;

public class Child2 extends Parent { //inherits from parent class

	public static void main(String[] args) {
		Child2 c2=new Child2();
		c2.method1();  // calling parent method1
		c2.method2();  // calling parent method2

	}

}
