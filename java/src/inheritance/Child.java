package inheritance;

public class Child  extends Parent{   //inherits parent class
	
	public void mymethod() {
		System.out.println("This is Child Class.....");
	}

	public static void main(String[] args) {
		Child c=new Child();
		c.method1();  // calling parent method1
		c.method2();  // calling parent method2
		

	}

}
