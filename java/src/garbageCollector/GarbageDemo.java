package garbageCollector;

public class GarbageDemo{
	//nullfying......referencing to another......anonymous object
	
	public void finalize() {
		System.out.println("unrefferenced object garbage collector..........");
	}

	public static void main(String[] args) {
		GarbageDemo gd= new GarbageDemo();
		gd=null;//nullfying
		
		GarbageDemo gd2= new GarbageDemo();
		//referencing to another
		gd=gd2;
		
		//anonymous object
		new GarbageDemo(); //no name 
		System.gc();
		
		

	}

}
