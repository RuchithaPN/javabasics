package main;

public class MainMethod {
	
	public static void main() {
		System.out.println("This is regular method.....");
	}
	public static void main(int a) {
		System.out.println("This is another method....");
	}

	public static void main(String[] args) {  //execution sarts from here
		// public---access to all
		//static--- call without an object
		//void--- return type----it doesn't return any value
		//main---name of method
		//String[]----array of string type arguments
		//args-- parameter name
		System.out.println("This is main method.....");
		main();
		main(50);

	}

}
