package impfeatures;

@FunctionalInterface
interface cab2{
	public  double method1(String start,String destination,int km,double rupees) ; // abstract method
		
	
}
public class LambdaDemo3 {

	public static void main(String[] args) {
		
		cab2 ref=(start,destination,km,rupees)->{
			System.out.println("Startrd from  " +start+"  to  "+destination); //
			return (km*rupees);
		};
		System.out.println(ref.method1("Karnataka", "Goa",150,20));

	}

}
