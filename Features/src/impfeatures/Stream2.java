package impfeatures;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Stream2 {

	public static void main(String[] args) {
		
		//list of names
		List<String> names=Arrays.asList("Sanju","Moulya","Adhya");
		// convert the list to stream
		List<String> names1=names.stream().filter(n->!"Adhya".equals(n)).collect(Collectors.toList());
		 // prints the resulted list
	       names1.forEach(System.out::println);
	}

}
