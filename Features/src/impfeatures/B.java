package impfeatures;

//creating  functional interface
@FunctionalInterface
interface MYInterface2{
	void method1();
	
	static void method2() {
		System.out.println("Static method...");
	}
}
public class B {

	public static void main(String[] args) {

		MYInterface2 my=()->System.out.println("Lambda Expression........");
		my.method1();   //  calling functional interface method
	}

}
