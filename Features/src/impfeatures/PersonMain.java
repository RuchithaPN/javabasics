package impfeatures;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PersonMain
{



   public static void main(String[] args)
    {
        //Storing data in list
        List<Person> persons = Arrays.asList(
                new Person("Anusha",24),
                new Person("Ruchi",23),
                new Person("Anju",24),
                new Person("Yashu",22));
        
        //Convert to Streams //Single Condition
        Optional SC = persons.stream().filter(n->"Anusha".equals(n.getName()))
        		.findAny();
           // if we use optional then we should not use  orElse(null)        		
        System.out.println(SC);
        
        //Convert to Streams //Multiple Condition
        Person MC = persons.stream().filter((n2)->!"Anusha".equals(n2.getName()) && 24 == n2.getAge()).findAny() .orElse(null);
        System.out.println(MC);
        
        List<Person>SM  = persons.stream().filter(n3->!"Anusha".equals(n3)).collect(Collectors.toList());
        System.out.println(SM);
    }



}