package impfeatures;
// Interface contains abstract methods.. Static methods... Default methods...

interface MyInterface{
	
	// Abstract method (without body)
	void method1();
	
	// static method
	static void method2() {
		System.out.println("Static method.......");
	}
	
	// Default method
	default void method3() {
		System.out.println("Default method.....");
	}
}


public class A implements MyInterface {
	
	@Override
	public void method1() {
		System.out.println("Overridden abstract method from MyInterface");
	}

	public static void main(String[] args) {
          
		A obj=new A();
		obj.method1();  //calling abstract method
		MyInterface.method2();   // calling static method by using Interface name
		obj.method3(); // calling static method
		
	}

}
