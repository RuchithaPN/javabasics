package impfeatures;

import java.util.ArrayList;
import java.util.List;

public class ForEachMethod2 {

	public static void main(String[] args) {
     List<String> demo=new ArrayList<>();
     
     demo.add("cat");
     demo.add("dog");
     demo.add("rabbit");
     demo.add("cow");
     demo.add("lion");
     
     //demo.forEach(System.out::println);
     demo.forEach(n->System.out.println(n));
	}

}
