package impfeatures;

@FunctionalInterface
interface Demo{  
	//abstract method
    public int method1(int a,int b); 
 
    // static method
 	static void method2() {
 		System.out.println("Static method.......");
 	}
 
}
  
public class LambdaDemo4 {

	public static void main(String[] args) {
		 
		
		        Demo ad2=(a, b)->{  
		        	System.out.println("Addition of"+a+"+"+b);
		                            return (a+b);
		                            };  
		        System.out.println(ad2.method1(100,200));  
		        
		        
		        Demo.method2();  // calling static method by using Interface name
	
		    }  
		} 