package impfeatures;

import java.util.Arrays;
import java.util.List;

public class ForEachMethod {

	public static void main(String[] args) {
     List<Integer> demo= Arrays.asList(10,20,30,40,50);
     System.out.println("List og Integers");
     demo.forEach(System.out::println);
	}

}
