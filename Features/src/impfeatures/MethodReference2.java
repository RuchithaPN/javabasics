package impfeatures;

@FunctionalInterface
interface DisplayInterface{
	void display();  // abstract method
	
}

public class MethodReference2 {
	
	public void sayHello() {   // instance method
		System.out.println("Helloooo....");
		}

	public static void main(String[] args) {
		MethodReference2 ob=new MethodReference2(); //object created
		DisplayInterface dis=ob::sayHello;   //method reference using object
		dis.display();  // calling the functional interface method

	}

}
