package impfeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamDemo {

	public static void main(String[] args) {
       List<String> names=new ArrayList<>();
       names.add("ruchitha");
       names.add("anjana");
       names.add("Yashu");
       names.add("Anusha");
       
 //      int count=0;                old method
 //      for(String str:names) {
 //   	   if(str.length()<=6)
 //   		   count++;
  //     }
 //      System.out.println(count+" Names with less than 5 characters");   
       // using stream and lambda
       long count=names.stream().filter(str->str.length()<=6).count();
       System.out.println(count+" Names with less than 5 characters"); 
       
       List<String> names1=names.stream().filter(str->length()<=6).collect(Collectors.toList();
       names1.forEach(System.out::println);
       
       // creating a stream using stream of
       Stream<String> names2=Stream.of("Sanju","Moulya");
       names2.forEach(System.out::println);
	}
	

}
