package junitTesting;

public class Multitest {

	public int add(int n1,int n2) {
		return n1+n2;

	}
	
	public double mult(double a,double b,double c) {
		return  a*b*c;
		
	}
	
	public double sub(double p,double q) {
		return p-q;
	}
	
	public float avg(float r,float s) {
		float f= (r+s)/2;
		return f;
	}
	
	public long div(long a,long b) {
		return a/b;
		
	}
	
	
}
