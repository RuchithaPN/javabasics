package junitTesting;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MTesting {
    @Test
    
    public void testAdd() {
	
	Multitest s=new Multitest();
	
	assertEquals(20, s.add(10,10));
	
}
   @Test
   @BeforeEach
 
   public void testMul() {
    	
    	Multitest s=new Multitest();
    	
    	assertEquals(1000, s.mult(10, 10, 10));
    	
    }
  
  @Test
  @BeforeAll
    
  static void testSub (){
	
	Multitest s=new Multitest();
	
	assertEquals(10, s.sub(30,20));
	
}
  @Test
  @AfterEach
  
  public void testAvg() {
  	
  	Multitest s=new Multitest();
  	
  	assertEquals(10, s.avg(10, 10));
  	
  }
  
  @Test
  @AfterAll
  
  static void testDiv() {
		
	  	Multitest s=new Multitest();
	  	
	  	assertEquals(2, s.div(6, 3));
	  	
	  }
  }
 
  
 
